<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en « wp-config.php » et remplir les
 * valeurs.
 *
 * Ce fichier contient les réglages de configuration suivants :
 *
 * Réglages MySQL
 * Préfixe de table
 * Clés secrètes
 * Langue utilisée
 * ABSPATH
 *
 * @link https://fr.wordpress.org/support/article/editing-wp-config-php/.
 *
 * @package WordPress
 */

// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define( 'DB_NAME', 'w_ebt' );

/** Utilisateur de la base de données MySQL. */
define( 'DB_USER', 'root' );

/** Mot de passe de la base de données MySQL. */
define( 'DB_PASSWORD', '' );

/** Adresse de l’hébergement MySQL. */
define( 'DB_HOST', 'localhost' );

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/**
 * Type de collation de la base de données.
 * N’y touchez que si vous savez ce que vous faites.
 */
define( 'DB_COLLATE', '' );

define( 'FS_METHOD', 'direct' );

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clés secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '%E}ljXR1`zJ6l8D8B~To1N9[YQ.f~q?FHCCUD}T$X3Hgtk2oPLhvG88kg]|h(N0I' );
define( 'SECURE_AUTH_KEY',  '%Q#`mB[wj1Eg7Vk)c;rw!v7CVVG5G-l)*2~Q[4_S3;rGUXw0EDKX>;B F$wb~o[K' );
define( 'LOGGED_IN_KEY',    'X4&O6af[ K36!D^m#LI U=IH/TD2h=B~ITS:ru1zx+;thBKQ.@K_lVr>}|aJP}n*' );
define( 'NONCE_KEY',        '1.&b5L4aNUe0[TM9rFbfVdAs;xY=M8{&f]-mzC~wW!f8KWyCsT-h`W}?b:Gi]+st' );
define( 'AUTH_SALT',        ';8woJw$2][V/e*rSV:Q-L?,0*FE8wYbCtmfA[FQFI,yqChl&tg^O5AzyY6]Bv:1F' );
define( 'SECURE_AUTH_SALT', 'ZS%S?P;R2B0E_+0FriGYy.2J6Y;KY:S>K{4xjKCWPU%0=eF40SK#?BT<b[tfOMcL' );
define( 'LOGGED_IN_SALT',   'krEaKUd!BI!pi%[>|n4}A7$~TANDI)-]2NN%~)NgxQCb2?p[7-DbaK:40kk<iXk~' );
define( 'NONCE_SALT',       '*b0[F:S8bSZ+)gX(F?$@s6i:kdL{na0<8QvL $l6w/~yiI23[cu|030y|ClKm@.7' );
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix = 'ebt_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortement recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://fr.wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* C’est tout, ne touchez pas à ce qui suit ! Bonne publication. */

/** Chemin absolu vers le dossier de WordPress. */
if ( ! defined( 'ABSPATH' ) )
  define( 'ABSPATH', dirname( __FILE__ ) . '/' );

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once( ABSPATH . 'wp-settings.php' );
